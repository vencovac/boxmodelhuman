function [x,Ginv,stiff,ShSp,undamp,bigamma,wm2,Qbm,Qs, MEinv, kME, hME, Gme, Sty, gammaAir, ...
      Vtc, GammaMi, Gs, Sow, W, Pec] = alldataRGME(N,gain)
%function [x,Ginv,stiff,ShSp,undamp,bigamma,wm2,Qbm,Qs, MEinv, kME, hME, Gme, Sty, gammaAir, ...
%      Vtc, GammaMi, Gs, Sow, W, Pec] = alldataRGME(N,gain)
%
%

if nargin <2, gain = 1.05; end % gain of the model
if nargin <1,N=800; end % number of sections

%________________________BM data_______________________________
	
[x,N,M,stiff,damp0,ShSp,G,Gs,Ginv,Qbm,Qs,MEinv,mME,kME,hME,Gme,Sty,gammaAir, ...
    Vtc,GammaMi,Sow,W,Pec] = bmdatargMEP(N);

[bigamma,wm2] = tmdatargP(x); 

Xzero = 1.0; % point where shaping function is 0
Sc = 6; % scale x axis for tanh - defines the shape of tanh function
%ScAmp = 0.1; % scale amplitude of tanh function
ScAmp = 0.12;
ScAmp = 0.15;

%UmpBas = 1.04+ScAmp*(tanh((Sc*(Xzero-x)/max(x)))); % g2
%UmpBas = 1.02+ScAmp*(tanh((Sc*(Xzero-x)/max(x)))); % g3
%UmpBas = 1.02+ScAmp*(tanh((Sc*(Xzero-x)/max(x)))); % g4
UmpBas = 1.02+ScAmp*(tanh((Sc*(Xzero-x)/max(x)))); % g2

%________________________________________________
%  undamp=1.34*damp0(:).*bigamma; % def
%shapeOn = hann(100);
%shapeAll = [shapeOn(1:50); ones(N-100,1); shapeOn(51:end)];
% undamp=1.34*gain*damp0(:).*bigamma.*shapeAll;
undamp=1.34*gain*damp0(:).*bigamma.*UmpBas';
stiff=stiff(:);


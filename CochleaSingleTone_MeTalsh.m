function [tone, tim, Y1FT, Y1ME, Pd, BMdisp,MEdisp,TMdisp,YTMFT]=CochleaSingleTone_MeTalsh(A1o,F1o,phi1o,Tonset,NumPer,gain,visual)
%function [tone, tim, Y1FT, Y1ME, Pd, BMdisp,MEdisp,TMdisp,YTMFT]=CochleaSingleTone_MeTalsh(A1o,F1o,phi1o,Tonset,NumPer,gain,visual)
%
% Run cochlear model + ME model for a pure tone as an input, onset ramp is
% set to 10 ms
%
% input parameters:
% A1o: dB SPL of the pure tone
% F1o: Frequency of the pure tone (Hz)
% phi1o: phase of the pure tone
% Tonset: Time after which the spectra (Y1FT, Y1ME and YTMFT) are
% calculated (to avoid onset transients)
% NumPer: number of periods of the steady state signal (after Tonset)
% gain: cochlear model gain (a real number which scales the gain)
% visual: 1 - plot BM vibrations in the real time, 0 - no plotting
%
% outputs:
% tone: the input pure tone
% tim: time
% Y1FT: BM displacement at the pure tone frequency (spectral component)
% Y1ME: Oval window displ. at the pure tone frequency (spectral component)
% Pd: pressure along the BM
% BMdisp: BM displacement
% MEdisp: oval window displacement
% TMdisp: shearing displacement between RL and TM
% YTMFT: shearing displ. between RL and TM at the pure tone frequency
% (spectr. comp.)
% 
% Vaclav Vencovsky, Ales Vetesnik
% vaclav.vencovsky@gmail.com

if nargin < 1,  A1o=50; end
if nargin < 2,  F1o=1500; end
if nargin < 3,  phi1o=0; end
if nargin < 4,  Tonset=25e-3;end
if nargin < 5,  NumPer=10; end
if nargin < 6,  gain=1.05; end
if nargin < 7,  visual=1; end

global Ginv DampSp  stiff bigamma wm2 undamp N MEinv kME hME Gme Sty gammaAir ...
      Ve GammaMi Gs Qbm Sow Wbm Pea
global A1 F1 phi1

ResetAll; %reset global

A1=A1o;F1=F1o;phi1=phi1o;

h=5e-6/3;   %time step
fs=1/h;   %sampling frequency
I=sqrt(-1);
im1=-I*2*pi*F1;


%________________________MODEL PARAMETERS_____________________________________%
 N=800;
 
 [x,Ginv,stiff,DampSp,undamp,bigamma,wm2,Qbm,Qs, MEinv, kME, hME, Gme, Sty, gammaAir, ...
     Ve, GammaMi, Gs, Sow, Wbm, Pea] = alldataRGME(N,gain);

NuSa = round(NumPer*fs/F1);

ts=Tonset; % plus onset ramp duration

Lp=fix(ts*fs)+NuSa;%+fix(ts*fs/2);
%________________________Graphics_________________________________


if visual % visualize simulation
t1 = 0:1/fs:(Lp-1)/fs;

Sig = zeros(Lp,1);
Ma = 1.1e0; % scale Y axis to visualize the traveling wave

for k=1:Lp
    [Sig(k)]=Inp_sigPureToneME(F1,A1,t1(k),phi1);
end
Ma1 = 1.1*max(Sig);
  
% vidObj = VideoWriter('BMdisp80dB.avi'); open(vidObj);

 H_FIG = figure('Name','HUMAN COCHLEA MODEL','DoubleBuffer','on','NumberTitle','off');
   H_BM=axes('Position', [.072 .1 .9 .4], 'Box', 'on','XLim',[x(1),x(N)],'YLim',[-Ma,Ma]);
      h_tit1 = text(-1, 0, 'BM displacement (red) and shearing (RL and TM) displacement (green)');
      set(H_BM, 'Title', h_tit1);
      h_L1 = line('XData',x,'YData',zeros(N,1),'Color', 'r');
    %  h_L3 = line('XData',x,'YData',zeros(N,1),'Color', 'g');
           xlabel('Distance from stapes [cm]');
         
   H_SIG =	axes('Box', 'on','Position',[0.072,0.675,0.9,0.25]);
   set(H_SIG,'Xlim', [0, t1(Lp)], 'Ylim', [-Ma1, Ma1]);
   h_tit2 = text(-1, 0, ['Input signal-->F1:=',num2str(F1),' [Hz], L1:=',num2str(A1),' [dB]'],'Tag','h_tit2');
   h_xlbl2 = text(-1,0, 'Time [sec]', 'Tag','h_xlbl2');
   h_ylbl2 = text(-1,0, 'Amplitude', 'Tag', 'h_ylbl2');
   set(H_SIG, 'Title', h_tit2,'XLabel', h_xlbl2, 'YLabel',h_ylbl2);
   h_L2 = line('XData',t1,'YData',Sig,'Erasemode','none','Color', 'b');
   h_L0=line('Xdata', [t1(1), t1(1)], 'Ydata', [-Ma1, Ma1]);   
   set(h_L0, 'Color', 'r');
   drawnow;
end
%  
 %_________________________RUNGE KUTTA____________________________________%
 n_c=1;             %statistic for graphics 
 t0=0;              %start time
 y0=zeros(4*N+3,1);   %initial conditions
 y=y0(:);
 t=t0;
 neq = length(y);
 %------------------------------------------- 
 pow = 1/5;
 A = [1/5; 3/10; 4/5; 8/9; 1; 1];
 B = [
    1/5         3/40    44/45   19372/6561      9017/3168       35/384
    0           9/40    -56/15  -25360/2187     -355/33         0
    0           0       32/9    64448/6561      46732/5247      500/1113
    0           0       0       -212/729        49/176          125/192
    0           0       0       0               -5103/18656     -2187/6784
    0           0       0       0               0               11/84
    ];

F = zeros(neq,6);


hA = h * A;
hB = h * B;
%____________________Main routine___________________________________%
Y1FT=zeros(N,1);
YTMFT=zeros(N,1);
Y1ME=0;

count=0;
tone = zeros(Lp,1);
BMdisp = zeros(Lp,N);
MEdisp = zeros(Lp,1);
TMdisp = zeros(Lp,N);
tim = zeros(Lp,1);
Pd = zeros(Lp,1);

while count<Lp   
                             
       F(:,1) = activeC(t,y);
       F(:,2) = activeC(t + hA(1), y + F*hB(:,1));
       F(:,3) = activeC(t + hA(2), y + F*hB(:,2));
       F(:,4) = activeC(t + hA(3), y + F*hB(:,3));
       F(:,5) = activeC(t + hA(4), y + F*hB(:,4));
       F(:,6) = activeC(t + hA(5), y + F*hB(:,5));
       
       II=Inp_sigPureToneME(F1,A1,t + hA(5),phi1);
 
       Pd(count+1)=-1/Wbm*Qs*F(4*N+2,6)-1/Wbm*Qbm*F(N+1:2*N,6);
       tim(count+1)=t + hA(5);
       tone(count+1) = II;
       t = t + hA(6);
       y = y + F*hB(:,6);
       
       
       if n_c==30&visual %graphics
           set(h_L1,'Ydata',y(1:N));
          % set(h_L3,'Ydata', y(2*N+1:3*N));
           set(h_L0, 'Xdata', [t, t], ...
             'Color', 'r');    
           n_c=0;
           
           drawnow; 
       %    currFrame = getframe(gcf);
      %     writeVideo(vidObj,currFrame);
       end
       n_c=n_c+1;
       count=count+1;
        
      BMdisp(count,:) = y(1:N); % get BM displacement
      MEdisp(count) = y(4*N+1); % oval windows displ.
      TMdisp(count,:) = y(2*N+1:3*N); % shearing displ between RL and TM
      
       if t>ts&&t<(ts+NuSa*h) %DFT
            Y1FT=Y1FT+y(1:N)*exp(im1*t);
            Y1ME=Y1ME+y(4*N+1)*exp(im1*t);
            YTMFT=YTMFT+y(2*N+1:3*N)*exp(im1*t);
 
       end
%                 
end
if visual, close(H_FIG);  end; %close(vidObj);
Y1FT=(2*Y1FT)/(NuSa);
Y1ME=(2*Y1ME)/(NuSa);
YTMFT=(2*YTMFT)/(NuSa);

    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%----------------------------UTILITY----------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%-----------------------------------------------------------%%     
function ResetAll

 global Ginv DampSp  stiff bigamma wm2 undamp N MEinv kME hME Gme Sty gammaAir ...
        Ve GammaMi Gs Qbm Sow Wbm Pea
 
 global  Sf A1 A2 F1 F2 phi1 phi2
 
 Ginv=[]; DampSp=[]; stiff=[]; bigamma=[]; wm2=[];
 MEinv=[]; kME = []; hME = []; Gme = []; Sty=[]; gammaAir = []; Ve = []; 
 GammaMi = []; Gs = []; Qbm = []; Sow = []; Wbm = []; Pea = [];
 
 undamp=[]; N=[];
 Sf=[]; A1=[]; A2=[];  F1=[]; F2=[]; phi1=[]; phi2=[];
 
%---------------------------------------
function dXdV = passiveC(t,XV)

  global BMinput Ginv DampSp  stiff  N   
  global om om2 fac 
  
   inp=-tanh(t*fac)*om2*cos(om*t);
   X=XV(1:N);
   V=XV(N+1:2*N);

   dV = -BMinput*inp-Ginv*(stiff.*X + DampSp*V); 

   dXdV=[V
          dV];

%---------------------------------------
function dXdVdYdW = activeC(t,XVYW)

            
global Ginv DampSp  stiff bigamma wm2 undamp N MEinv kME hME Gme Sty ...
        Ve GammaMi Gs Qbm Sow Wbm Pea
global A1 F1 phi1
 
[inp]=Inp_sigPureToneME(F1,A1,t,phi1);
    
    X=XVYW(1:N);
    V=XVYW(N+1:2*N);
    Y=XVYW(2*N+1:3*N);  
    W=XVYW(3*N+1:4*N);
    Xme = XVYW(4*N+1);
    Vme = XVYW(4*N+2);

   Ycut=nonlin(Y);

   dV = -1*Ginv*(stiff.*X + DampSp*V + undamp.*Ycut - Gs*MEinv*hME*Vme - Gs*MEinv*kME*Xme + Gs*MEinv*Sow*Gme*inp);
   dVme = -1*MEinv*( hME*Vme + kME*Xme + Sow*Qbm*dV/Wbm - Sow*Gme*inp);
    
   Pe = inp - Pea*Sty*GammaMi/Ve*Xme;
    
   dXdVdYdW=[ V  %1:N
               dV %N+1:2*N
                W %2*N+1:3*N
                -bigamma.*W - wm2.*Y - dV % 3*N+1:4*N
                 Vme % 4*N+1
                  dVme % 4*N+2
                    Pe]; % 4*N+3            

 

function [Sig]=Inp_sigPureToneME(F1,A1,t,phi1)

om1=2*pi*F1;
AM1=db2inputME(A1);%*om1^2;   %second time derivative
    
durHW = 10e-3; % duration of ramp
 
Sig=m_hann(t,durHW)*((AM1.*cos(om1*t+phi1)));
AM=max(abs(Sig));
     
end
 
function w=m_hann(t,dur)
    framp = 1/(2*dur);
    if t<=dur
        w = 0.5 * (1 - cos(2*pi*t*framp));  
    else
        w = 1;
    end
end
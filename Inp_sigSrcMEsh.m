function [Sig]=Inp_sigSrcMEsh(F1,A1,F2,A2,t,phi1,phi2,T2on,T2off,T1off)
% function [Sig]=Inp_sigSrcMEsh(F1,A1,F2,A2,t,phi1,phi2,T2on,T2off,T1off)
% generate input tone (two tones)
% 
    om1=2*pi*F1;om12=om1^2;
    AM1=db2inputME(A1);%*om1^2;   %second time derivative
    om2=2*pi*F2;om22=om2^2;
    AM2=db2inputME(A2);%*om2^2;   %second time derivative
    %omdp=2*pi*(2*F1-F2);
    durHW = 10e-3; % duration of ramp
    durHWF1 = 4e-3; % duration of F1 ramp
      
    Sig=m_hann(t,durHW)*((AM1.*cos(om1*t+phi1)+AM2.*cos(om2*t+phi2)));
    AM=max(abs(Sig));  

    Sig=0;
    if t<T2on  
        Sig=m_hann(t,durHWF1)*AM1*cos(om1*t+phi1);
    elseif (t>=T2on)&&(t<T2off-durHW)
        Sig=m_hann(t,durHWF1)*AM1*cos(om1*t+phi1)+...
        m_hann(t-T2on,durHW)*AM2*cos(om2*t+phi2);
    elseif (t>=T2off-durHW)&&(t<T2off)    
        Sig=m_hann(t,durHWF1)*AM1*cos(om1*t+phi1)+...
        m_hann(T2off-t,durHW)*AM2*cos(om2*t+phi2);
    elseif (t>=T2off)&&(t<T1off-durHWF1)
        Sig=AM1*cos(om1*t+phi1);
    elseif (t>=T1off-durHWF1)&&(t<T1off)
        Sig=m_hann(T1off-t,durHWF1)*AM1*cos(om1*t+phi1);
    else
        Sig=0;
    end
     

 
function w=m_hann(t,dur)
      framp = 1/(2*dur);
      if t<=dur
        w = 0.5 * (1 - cos(2*pi*t*framp));  
      else
        w = 1;
      end
% script to simulate DPOAEs
% A1List and A2List can be provided as vectors with more values thus the
% script can calculate more L1 and L2 values for DPOAEs in one run
% DPOAEs are simualted in the smooth cochlear model
% visual - 1 (for visualization of simulation),  - 0 (no visualization)


A1List = 58;  % L1 levels in dB
A2List = 55;  % L2 levels in dB


F1o = 2000;  % F1 frequency in Hz
F2o = 2400;  % F2 frequency in Hz
Fdp = 2*F1o - F2o;  % 2F1 - F2 freq


fs = 600e3;
% for simulation
Tdur = 40e-3;
Tan = 19e-3; % start of analysis window
Nan = round(Tan*fs);
Nwin = 6000;  % the window is set to 10 ms for FFT calculatoin without speactral leakage
T2on = 0; % onset of F2 tone
T2off = Tdur; % offset of F2 tone
T1off = Tdur; % offset of F1 tone

Ma=4e-2;
gain=1.05; % this varaiable controls the amount of gain, it multiplies the undampoing feedback force

S=0; % 1 - normal measurement, 2 - suppression
phi1o=0; 
phi2o=0;
visual = 1;
FolderRes = 'Results/';


Fdp=2*F1o-F2o; %[Hz]
fx = linspace(0,fs-1/fs,Nwin);    
idxFdp = find(round(fx)==Fdp);
idxF1= find(round(fx)==F1o);
idxF2= find(round(fx)==F2o);


Nonset = round(Tan*fs);  % number of samples in the onset





for k1=1:length(A1List)
    
    A1o = A1List(k1);
    
    
    for k2=1:length(A2List)
        
        A2o = A2List(k2);
                  
        [Y1FT,Y2FT,YDP1FT,YDP2FT,oae1,tim,x,RR,UU,UUr]=CochleaRGB_DP_DFT_MEtalNLsh(A1o,F1o,A2o,F2o,Tdur,Ma,gain,phi1o,phi2o,T2on,T2off,T1off,visual);%RR...
                
        UUrdpF = fft(UUr(:,Nan:Nwin+Nan-1).')/Nwin;
        RRF = fft(RR(:,Nan:Nwin+Nan-1).')/Nwin;
        UUrdp{k1,k2} = 2*UUrdpF(idxFdp,:).*exp(-sqrt(-1)*2*pi*Fdp*Nonset*1/fs); 
        RR1{k1,k2} = 2*RRF(idxF1,:).*exp(-sqrt(-1)*2*pi*F1o*Nonset*1/fs);
        RR2{k1,k2} = 2*RRF(idxF2,:).*exp(-sqrt(-1)*2*pi*F2o*Nonset*1/fs);
        RRdp{k1,k2} = 2*RRF(idxFdp,:).*exp(-sqrt(-1)*2*pi*Fdp*Nonset*1/fs);
        oaeF = fft(oae1(Nan:Nwin+Nan-1))/Nwin;
        oaedp(k1,k2) = 2*oaeF(idxFdp).*exp(-sqrt(-1)*2*pi*Fdp*Nonset*1/fs);
        
        
    end  % example how results can be saved...
       % save([FolderRes 'JASAEL_R_dpL1L2mapL1' num2str(A1o)   'F1_' num2str(F1o) 'Hz_F2_' num2str(F2o) 'Hz_gain' num2str(gain*100) 'R' num2str(rough) '.mat'],...
       %    'UUrdp','oaedp','x','A1List','A2List','F1o','F2o','RR1','RR2','RRdp');

 end

     %save([FolderRes 'JASAEL_R_dpL1L2mapF1_' num2str(F1o) 'Hz_F2_' num2str(F2o) 'Hz_gain' num2str(gain*100) 'R' num2str(rough) '.mat'],...
     %      'UUrdp','oaedp','x','A1List','A2List','F1o','F2o','RR1','RR2','RRdp');




%metaparpool('close');

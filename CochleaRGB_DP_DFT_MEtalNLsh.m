function [Y1FT,Y2FT,YDP1FT,YDP2FT,oae,tim,x,RR,UU,UUr,TMdisp]=CochleaRGB_DP_DFT_MEtalNLsh(A1o,F1o,A2o,F2o,Tdur,Ma,gain,phi1o,phi2o,T2ono,T2offo,T1offo,visual)
%function [Y1FT,Y2FT,YDP1FT,YDP2FT,oae,tim,x,RR,UU,UUr,TMdisp]=CochleaRGB_DP_DFT_MEtalNLsh(A1o,F1o,A2o,F2o,Tdur,Ma,gain,phi1o,phi2o,T2ono,T2offo,T1offo,visual)
% Script which runs the model and gives TWs: Y1FT, Y2FT, YDP1FT, YDP2FT, pressure in the auditory canal
% (oae), BM displacement (RR), force
% of the cochlear amplifier (UU), nonlinear part of this force (UUr) and
% the shearing displacement between TM and RL (TMdisp).
%
% INPUT PARAMETERS
% A1o: L1 in dB SPL
% F1o: f1 in Hz
% A2o: L2 in dB SPL
% F2o: f2 in Hz
% Tdur: duration of the overall signal in seconds
% Ma: scaling of the y axis in graph showing the BM vibrations
% gain: controls the gain of the cochlear amplifier
% phi1o: phase of the f1 tone
% phi2o: phase of the f2 tone
% T2ono: onset of the f2 tone
% T2offo: offset of the f2 tone
% T1offo: offste of the f1 tone
% visual: 1 - show visualization, 0 - do not show anything
%
% FUNCTION OUTPUT
% Y1FT: envelope and phase of TW at F1 frequency (Freq domain result)
% Y2FT: envelope and phase of TW at F2 frequency (Freq domain result)
% YDP1FT: envelope and phase of TW at 2F1-F2 frequency (Freq domain result)
% YDP2FT: envelope and phase of TW at 2F2-F1 frequency (Freq domain result)
% oae: time domain pressure at the eardrum, which should contain DPOAE 
% tim: time samples
% x: distance from the stapes in cm, BM length is 3.5 cm
% RR: BM displacement in each model segment (time domain solution)
% UU: Undamping feedback force in each model segment (time domain soulution)
% UUr: Nonlinear force in each model segment (nonlinear component of feedback force, time domain solution)
% TMdisp: Displacement of second array of oscillators used to calculate undamping feedback force (shearing displacement)
% Vaclav Vencovsky, Ales Vetesnik
% vaclav.vencovsky@gmail.com

if nargin < 1,  A1o=50; end 
if nargin < 2,  F1o=2700; end
if nargin < 3,  A2o=50; end
if nargin < 4,  F2o=3400; end
if nargin < 5,  Tdur=150e-3;end
if nargin < 6,  Ma=2e-2; end
if nargin < 7,  gain=1.05; end
if nargin < 8,  phi1o=0; end
if nargin < 9,  phi2o=0; end
if nargin < 10, T2ono = 0.010; end;
if nargin < 11, T2offo = 0.130; end;
if nargin < 12, T1offo = 0.150; end;
if nargin < 13, visual = 1; end;

global Ginv DampSp  stiff bigamma wm2 undamp N MEinv kME hME Gme Sty gammaAir ...
      Ve GammaMi Gs Qbm Sow Wbm Pea
  
global A1 A2 F1 F2 phi1 phi2 T2on T2off T1off

ResetAll; %reset global

A1=A1o;A2=A2o;F1=F1o;F2=F2o;phi1=phi1o; phi2=phi2o; 
T2on = T2ono; T2off = T2offo; T1off = T1offo;

h=5e-6/3;   %time step
fs=1/h;   %sampling frequency
I=sqrt(-1);
im1=-I*2*pi*F1;
im2=-I*2*pi*F2;

Fdp1=2*F1-F2;
Fdp2=2*F2-F1;
    
omdp1=2*pi*Fdp1;imdp1=-I*omdp1;
omdp2=2*pi*Fdp2;imdp2=-I*omdp2;

%________________________MODEL PARAMETERS_____________________________________%
 N=800;

 [x,Ginv,stiff,DampSp,undamp,bigamma,wm2,Qbm,Qs, MEinv, kME, hME, Gme, Sty, gammaAir, ...
      Ve, GammaMi, Gs, Sow, Wbm, Pea] = alldataRGME(N,gain);
  
NC=80; 
ts=NC*(1/F1);
%Lp=fix((0.08+0.01*2)*fs);
Lp = fix(Tdur*fs);
t1=(0:Lp-1)*h;
Sig = zeros(size(t1));
for k=1:length(t1)
    [Sig(k)]=Inp_sigSrcMEsh(F1,A1,F2,A2,t1(k),phi1,phi2,T2on,T2off,T1off);
end

Ma1 = max(abs(Sig));
Ma1=Ma1*1.1;
Lp=length(t1);
%________________________Graphics_________________________________
 y=zeros(size(x));
if visual 
 H_FIG = figure('Name','HUMAN COCHLEA MODEL','DoubleBuffer','on','NumberTitle','off');
   H_BM=axes('Position', [.072 .1 .9 .4], 'Box', 'on','XLim',[x(1),x(N)],'YLim',[-Ma,Ma]);
      h_tit1 = text(-1, 0, 'BM displacement');
      set(H_BM, 'Title', h_tit1);
      h_L1 = line('XData',x,'YData',zeros(N,1),'Color', 'r');
           xlabel('Distance from stapes [cm]');
         
   H_SIG =	axes('Box', 'on','Position',[0.072,0.675,0.9,0.25]);
   set(H_SIG,'Xlim', [0, t1(Lp)], 'Ylim', [-Ma1, Ma1]);
   h_tit2 = text(-1, 0, ['Input signal-->F1:=',num2str(F1),' F2:=',num2str(F2) ' [Hz]'],'Tag','h_tit2');
   h_xlbl2 = text(-1,0, 'Time [sec]', 'Tag','h_xlbl2');
   h_ylbl2 = text(-1,0, 'Amplitude', 'Tag', 'h_ylbl2');
   set(H_SIG, 'Title', h_tit2,'XLabel', h_xlbl2, 'YLabel',h_ylbl2);
   h_L2 = line('XData',t1,'YData',Sig,'Erasemode','none','Color', 'b');
   h_L0=line('Xdata', [t1(1), t1(1)], 'Ydata', [-Ma1, Ma1]);   
   set(h_L0, 'Color', 'r');
   drawnow;
end
 %_________________________RUNGE KUTTA____________________________________%
 n_c=1;             %statistic for graphics 
 t0=0;              %start time
 y0=zeros(4*N+3,1);   %initial conditions
 y=y0(:);
 t=t0;
 neq = length(y);
 %------------------------------------------- 
 pow = 1/5;
 A = [1/5; 3/10; 4/5; 8/9; 1; 1];
 B = [
    1/5         3/40    44/45   19372/6561      9017/3168       35/384
    0           9/40    -56/15  -25360/2187     -355/33         0
    0           0       32/9    64448/6561      46732/5247      500/1113
    0           0       0       -212/729        49/176          125/192
    0           0       0       0               -5103/18656     -2187/6784
    0           0       0       0               0               11/84
    ];

F = zeros(neq,6);


hA = h * A;
hB = h * B;
%____________________Main routine___________________________________%
Y1FT=zeros(N,1);
Y2FT=zeros(N,1);
YDP1FT=zeros(N,1);
YDP2FT=zeros(N,1);
oae = zeros(Lp,1);
tim = zeros(Lp,1);

count=0;
countDFT=0;
ind1=1;%find(x>0.8);
ind2=N;%find(x>2,1,'first');

RR = zeros(N,Lp);
UU = zeros(N,Lp);
UUr = zeros(N,Lp);
TMdisp = zeros(N,Lp);

while count<Lp   
                             
       F(:,1) = activeC(t,y);
       F(:,2) = activeC(t + hA(1), y + F*hB(:,1));
       F(:,3) = activeC(t + hA(2), y + F*hB(:,2));
       F(:,4) = activeC(t + hA(3), y + F*hB(:,3));
       F(:,5) = activeC(t + hA(4), y + F*hB(:,4));
       F(:,6) = activeC(t + hA(5), y + F*hB(:,5));
       
       dy=F*hB(:,6);
       
       oae(count+1) = F(4*N+3,6); 
       
       
       tim(count+1)=t + hA(5);
        
       t = t + hA(6);
       y = y + F*hB(:,6);
       
       RR(:,count+1)=y(ind1:ind2);%\ksi BM displacement?
       %2*N+1:3*N % pro TM disp
       UU(:,count+1)= undamp(ind1:ind2).*nonlin(y(2*N+ind1:2*N+ind2));%U?
       UUr(:,count+1)= undamp(ind1:ind2).*(nonlin(y(2*N+ind1:2*N+ind2))-y(2*N+ind1:2*N+ind2));%U?
       TMdisp(:,count+1) = y(2*N+ind1:2*N+ind2);
              
       if n_c==8&&visual %graphics
           set(h_L1,'Ydata',y(1:N));
           set(h_L0, 'Xdata', [t, t], ...
             'Color', 'r');    
           n_c=0;
           drawnow; 
       end
       n_c=n_c+1;
       count=count+1;
      
      
       if (t>0.05) && (countDFT<=60e3-1)%DFT for freq. domain solutions
            Y1FT=Y1FT+y(1:N)*exp(im1*t);
            Y2FT=Y2FT+y(1:N)*exp(im2*t);
            YDP1FT=YDP1FT+y(1:N)*exp(imdp1*t);
            YDP2FT=YDP2FT+y(1:N)*exp(imdp2*t);
            countDFT=countDFT+1;
       end
                
end
if visual, close(H_FIG); end;
Y1FT=(2*Y1FT)/(countDFT);
Y2FT=(2*Y2FT)/(countDFT);
YDP1FT=(2*YDP1FT)/(countDFT);
YDP2FT=(2*YDP2FT)/(countDFT);
if nargout<1
    Y1FT=[];Y2FT=[];YDP1FT=[];YDP2FT=[];
end

    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%----------------------------UTILITY----------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%-----------------------------------------------------------%%     
function ResetAll

 global Ginv DampSp  stiff bigamma wm2 undamp N MEinv kME hME Gme Sty gammaAir ...
        Ve GammaMi Gs Qbm Sow Wbm Pea
 
 global  Sf A1 A2 F1 F2 phi1 phi2 T2on T2off T1off
 
 Ginv=[]; DampSp=[]; stiff=[]; bigamma=[]; wm2=[];
 MEinv=[]; kME = []; hME = []; Gme = []; Sty=[]; gammaAir = []; Ve = []; 
 GammaMi = []; Gs = []; Qbm = []; Sow = []; Wbm = [];   Pea = [];
 T2on = []; T2off = []; T1off = [];
 
 undamp=[]; N=[];
 Sf=[]; A1=[]; A2=[];  F1=[]; F2=[]; phi1=[]; phi2=[];
    
  
%---------------------------------------
function dXdV = passiveC(t,XV)

  global BMinput Ginv DampSp  stiff  N   
  global om om2 fac 
  
   inp=-tanh(t*fac)*om2*cos(om*t);
   X=XV(1:N);
   V=XV(N+1:2*N);

   dV = -BMinput*inp-Ginv*(stiff.*X + DampSp*V); 

   dXdV=[V
          dV];

%---------------------------------------
 function dXdVdYdW = activeC(t,XVYW)

global Ginv DampSp  stiff bigamma wm2 undamp N MEinv kME hME Gme Sty gammaAir ...
        Ve GammaMi Gs Qbm Sow Wbm Pea
    
global A1 A2 F1 F2 phi1 phi2 T2on T2off T1off
 
   [inp]=Inp_sigSrcMEsh(F1,A1,F2,A2,t,phi1,phi2,T2on,T2off,T1off);
   
      
    X=XVYW(1:N);
    V=XVYW(N+1:2*N);
    Y=XVYW(2*N+1:3*N);
    W=XVYW(3*N+1:4*N);
    Xme = XVYW(4*N+1);
    Vme = XVYW(4*N+2);
    

    Ycut=nonlin(Y);
    
    dV = -1*Ginv*(stiff.*X + DampSp*V + undamp.*Ycut - Gs*MEinv*hME*Vme - Gs*MEinv*kME*Xme + Gs*MEinv*Sow*Gme*inp);
    dVme = -1*MEinv*(hME*Vme + kME*Xme + Sow*Qbm*dV/Wbm - Sow*Gme*inp);
    
    Pe = inp - gammaAir*Pea*Sty*GammaMi/Ve*Xme;
        
    dXdVdYdW=[ V                          %N
               dV                         %2N 
               W                          %3N
               -bigamma.*W - wm2.*Y - dV  %4N
               Vme                        %4N+1   
               dVme                       %4N+2 
               Pe                         %4N+3
                ];            


 

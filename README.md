# BoxModelHuman

## Description
BoxModelHuman is a project containing scripts for the time domain implementation of a cochlear model, as used in the paper:

Vencovský V, Zelle D, Dalhoff E, Gummer AW, Vetešník A. The influence of distributed source regions in the formation of the nonlinear distortion component of cubic distortion-product otoacoustic emissions. J Acoust Soc Am. 2019 May;145(5):2909. doi: 10.1121/1.5100611. Erratum in: J Acoust Soc Am. 2019 Jul;146(1):381. doi: 10.1121/1.5119132. PMID: 31153314.

## Table of Contents
- [Installation](#installation)
- [Usage](#usage)
- [Contributing](#contributing)
- [License](#license)
- [Contact](#contact)

## Installation
To get started with BoxModelHuman, follow these steps:

1. Clone the repository:
    ```bash
    git clone https://gitlab.com/vencovac/boxmodelhuman.git
    ```
2. Change to the project directory:
    ```bash
    cd boxmodelhuman
    ```
3. Ensure you have MATLAB installed as the scripts are intended to be run in the MATLAB environment.

## Usage
To simulate Distortion Product Otoacoustic Emissions (DPOAEs), run the script `mainDPOAErun.m` in MATLAB:

1. Open MATLAB.
2. Navigate to the project directory.
3. Run the following command in the MATLAB command window:
    ```matlab
    mainDPOAErun
    ```
This will execute the simulation as described in the referenced paper.

## Contributing
We welcome contributions to improve BoxModelHuman. To contribute, please follow these steps:

1. Fork the repository.
2. Create a new branch (`git checkout -b feature-branch`).
3. Make your changes.
4. Commit your changes (`git commit -am 'Add new feature'`).
5. Push to the branch (`git push origin feature-branch`).
6. Create a new Pull Request.

## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Contact
For questions or feedback, please reach out to:

- GitLab: [vencovac](https://gitlab.com/vencovac)
- Email: vencovac@fel.cvut.cz
